import './App.css';
import {  WebsoketProvider , socket } from './contexts/WebsocketContext';

function App() {
  return (
    <WebsoketProvider value={socket}>

    </WebsoketProvider>
  );
}

export default App;
