import { Module } from '@nestjs/common';
import { GatewayController } from './gateway.controller';
import { MyGateWay } from './class/gateway';
@Module({
  controllers: [GatewayController],
  imports: [MyGateWay],
})
export class GatewayModule {}
